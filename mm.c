/*
 * mm.c - a x86 seglist malloc package
 *
 * The allocator manipulates the free blocks with segregated list (seglist).
 * Eight seglists exists, where the size of blocks in seglists are
 * -     ~   8 dwords
 * -   9 ~  16 dwords
 * -  17 ~  32 dwords
 * -  33 ~  64 dwords
 * -  65 ~ 128 dwords
 * - 129 ~ 256 dwords
 * - 257 ~ 512 dwords
 * - 513 ~     dwords
 * respectively. The seglist with bigger blocks have bigger index. The pointer
 * to each seglists are stored in the heap before any other blocks. The
 * prologue block comes right after the seglists area. At the highest end of
 * the heap, epilogue blocks are located. Memory allocation will happen on the
 * area between prologue and epilogue. Prologue and epilogue are filled with
 * 0x1, which means empty allocated block. Main purpose of the prologue and
 * epilouge is for comfortable coalesce. The entire structure of the heap is
 * in Figure 1.
 *
 *
 * Figure 1. Structure of the entire heap area.
 *
 * +------------------+----------+-------------------------------+-----------+
 * | list of seglists | prologue |     memory allocated here     | epilogue  |
 * | (4*8 byte)       | (4 byte) |                               | (12 byte) |
 * +------------------+----------+-------------------------------+-----------+
 * ^                                                                         ^
 * mem_heap_lo                                                     mem_heap_hi
 *
 *
 * The structures of the blocks are in Figure 2. Since free blocks consists in
 * seglist while allocated blocks are not, there structures are slightly
 * different. The free blocks have two pointers that pointing to previous and
 * next block in seglist respectively.
 *
 *
 * Figure 2. Structure of a free block (left) and an allocated block (right).
 *
 * +----------------------+-------+          +-----------------------+-------+
 * |         size         | 0 0 0 |<-header->|         size          | 0 0 1 |
 * +----------------------+-------+          +-----------------------+-------+
 * |   ptr to prev block header   |   block->|                               |
 * +------------------------------+     ptr  |                               |
 * |   ptr to next block header   |          |                               |
 * +------------------------------+          |           payload             |
 * |                              |<-block   |                               |
 * |                              |  ptr     |                               |
 * |                              |          |                               |
 * |          free space          |          |                               |
 * |                              |          +-------------------------------+
 * |                              |          |           optional            |
 * |                              |          |           paddings            |
 * +----------------------+-------+          +-----------------------+-------+
 * |         size         | 0 0 0 |<-footer->|         size          | 0 0 1 |
 * +----------------------+-------+          +-----------------------+-------+
 * 32 bits                3       0          32 bits                 3       0
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>

#include "mm.h"
#include "memlib.h"

/*********************************************************
 * NOTE TO STUDENTS: Before you do anything else, please
 * provide your information in the following struct.
 ********************************************************/
student_t student = {
  /* Your full name */
  "Seokjin Han",
  /* Your student ID */
  "2012-11853",
};

/* DON'T MODIFY THIS VALUE AND LEAVE IT AS IT WAS */
static range_t **gl_ranges;

/* single word (4) or double word (8) alignment */
#define WSIZE 4
#define DSIZE 8
#define ALIGNMENT DSIZE

/* heap boundary */
#define BOUND_LO (mem_heap_lo())
#define BOUND_HI (mem_heap_hi()+1)

/* rounds up to the nearest multiple of ALIGNMENT */
#define ALIGN(size) (((size) + (ALIGNMENT-1)) & ~0x7)
#define SIZE_T_SIZE (ALIGN(sizeof(size_t)))

#define MAX(x, y) ((x) > (y) ? (x) : (y))

/* pack block size and block allocation info */
#define PACK(size, alloc) ((size) | (alloc))

/* get/put 4 byte values from/to given address */
#define GET(p) (*(unsigned int *)(p))
#define PUT(p, val) (*(unsigned int *)(p) = (unsigned int)(val))

/* get block infos from header/footer */
#define GET_SIZE(p) (GET(p) & ~0x7)
#define GET_ALLOC(p) (GET(p) & 0x7)

/* allocated/free block pointer to block header */
#define FBP_TO_HDR(fbp) ((fbp) - 3*WSIZE)
#define ABP_TO_HDR(abp) ((abp) - WSIZE)

/* header <-> footer conversion */
#define HDR_TO_FTR(hdr) ((hdr) + GET_SIZE(hdr) - WSIZE)
#define FTR_TO_HDR(ftr) ((ftr) - GET_SIZE(ftr) + WSIZE)

/* get various addresses from block header */
#define HDR_TO_ABP(hdr) ((hdr) + WSIZE)
#define HDR_TO_FBP(hdr) ((hdr) + 3*WSIZE)
#define HDR_TO_PREV(hdr) ((hdr) + WSIZE)
#define HDR_TO_NEXT(hdr) ((hdr) + 2*WSIZE)

/* get adjacdent block header/footer from given block footer/header */
#define FTR_TO_ADJ_HDR(ftr) ((ftr) + WSIZE)
#define HDR_TO_ADJ_FTR(hdr) ((hdr) - WSIZE)

/* seglists */
#define N_SEGLIST 8
static void **seglists;

/* declare local functions */
static int size_to_index(size_t size);
static void remove_from_seglist(void *hdr);
static void insert_to_seglist(void *hdr);
static void *coalesce(void *hdr);
static void *extend_heap(size_t size);
static void *find_fit(size_t size);
static void place(void *hdr, size_t size);

/*
 * remove_range - manipulate range lists
 * DON'T MODIFY THIS FUNCTION AND LEAVE IT AS IT WAS
 */
static void remove_range(range_t **ranges, char *lo)
{
  range_t *p;
  range_t **prevpp = ranges;

  if (!ranges)
    return;

  for (p = *ranges;  p != NULL; p = p->next) {
    if (p->lo == lo) {
      *prevpp = p->next;
      free(p);
      break;
    }
    prevpp = &(p->next);
  }
}

/*
 * handle_double_free - prints an error message and aborts
 *    when your program tries to free an unallocated or freed
 *    memory block.
 * DON'T MODIFY THIS FUNCTION AND LEAVE IT AS IT WAS
 */
void handle_double_free(void) {
  printf("ERROR : Your program tried to unallocated or freed memory block.\n");
  exit(-1);
}

/*
 * mm_check - heap consistency checker.
 * the checker should be called at the end of place, coalesce, and mm_exit,
 * when checking the heap consistency.
 * this checker checks
 * - block alignments
 * - equality of heap size and sum of block sizes
 * - validity of heap prologue and epilogue
 * - validity of pointers in list of seglists
 * - validity of block header and footer
 * - validity of seglists and free blocks
 * - existence of adjacent free blocks that escaped coalescing
 * - existence of noncontiguous blocks
 */
int mm_check(void)
{
  int i;
  void *curr_hd, *curr_ft, *next_hd;
  void *free_curr[N_SEGLIST], *free_next[N_SEGLIST];
  void *lo = BOUND_LO + ALIGN(WSIZE*N_SEGLIST);
  void *hi = BOUND_HI;
  // check validity of heap prologue and epilogue
  assert(GET(lo) == 0x1);
  lo += WSIZE;
  for (i=0; i<3; ++i) {
    hi -= WSIZE;
    assert(GET(hi) == 0x1);
  }
  // check validity of pointers in list of seglists
  for (i=0; i<N_SEGLIST; ++i) {
    free_curr[i] = NULL;
    if (seglists[i] != NULL)
      assert(seglists[i] <= (void *)GET(HDR_TO_PREV(seglists[i])));
  }
  curr_hd = lo;
  while (curr_hd < hi) {
    curr_ft = HDR_TO_FTR(curr_hd);
    next_hd = FTR_TO_ADJ_HDR(curr_ft);
    // check validity of block header and footer
    assert(GET(curr_hd) == GET(curr_ft));
    assert(lo <= curr_hd);
    assert(curr_ft < hi);
    if (GET_ALLOC(curr_hd)) {
      // check block alignments of allocated blocks
      assert((unsigned int)HDR_TO_ABP(curr_hd)%ALIGNMENT == 0);
    } else {
      // check block alignments of free blocks
      assert((unsigned int)HDR_TO_FBP(curr_hd)%ALIGNMENT == 0);
      // check existence of adjacent free blocks
      assert(GET_ALLOC(curr_hd) != GET_ALLOC(next_hd));
      i = size_to_index(GET_SIZE(curr_hd));
      // check validity of seglists and free blocks
      if (free_curr[i] != NULL) {
        assert(curr_hd == free_next[i]);
        assert(free_curr[i] == (void *)GET(HDR_TO_PREV(curr_hd)));
      } else {
        assert(seglists[i] != NULL);
      }
      free_curr[i] = curr_hd;
      free_next[i] = (void *)GET(HDR_TO_NEXT(free_curr[i]));
      if (free_next[i] < free_curr[i]) {
        assert(free_curr[i] == (void *)GET(HDR_TO_PREV(seglists[i])));
        assert(free_next[i] == seglists[i]);
      }
    }
    curr_hd = next_hd;
  }
  // check equality of heap size and sum of block sizes
  assert(curr_hd = hi);
  return 1;
}

/*
 * size_to_index - find a proper seglist bin from size, returns -1 if zero.
 * bin sizes of seglists spaced exponentially.
 */
static int size_to_index(size_t size)
{
  if (size == 0) return -1;
  size = size/ALIGNMENT;
  if (size <= 8) return 0;
  else if (size <= 16) return 1;
  else if (size <= 32) return 2;
  else if (size <= 64) return 3;
  else if (size <= 128) return 4;
  else if (size <= 256) return 5;
  else if (size <= 512) return 6;
  return 7;
}

/*
 * remove_from_seglist - remove a given free block from corresponding seglist.
 */
static void remove_from_seglist(void *hdr)
{
  int idx = size_to_index(GET_SIZE(hdr));
  if (hdr == (void *)GET(HDR_TO_NEXT(hdr))) {
    seglists[idx] = NULL;
  } else {
    PUT(HDR_TO_NEXT(GET(HDR_TO_PREV(hdr))), GET(HDR_TO_NEXT(hdr)));
    PUT(HDR_TO_PREV(GET(HDR_TO_NEXT(hdr))), GET(HDR_TO_PREV(hdr)));
    if (hdr == seglists[idx]) seglists[idx] = (void *)GET(HDR_TO_NEXT(hdr));
  }
}

/*
 * insert_to_seglist - insert a given free block to corresponding seglist.
 */
static void insert_to_seglist(void *hdr)
{
  int idx = size_to_index(GET_SIZE(hdr));
  if (seglists[idx] == NULL) {
    seglists[idx] = hdr;
    PUT(HDR_TO_NEXT(hdr), hdr);
    PUT(HDR_TO_PREV(hdr), hdr);
  } else {
    void *tmp = seglists[idx];
    if (hdr < tmp || hdr > (void *)GET(HDR_TO_PREV(tmp))) {
      if (hdr < tmp) seglists[idx] = hdr;
      PUT(HDR_TO_NEXT(hdr), tmp);
      PUT(HDR_TO_PREV(hdr), GET(HDR_TO_PREV(tmp)));
      PUT(HDR_TO_NEXT(GET(HDR_TO_PREV(tmp))), hdr);
      PUT(HDR_TO_PREV(tmp), hdr);
    } else {
      while (tmp != (void *)GET(HDR_TO_PREV(seglists[idx]))) {
        if (tmp < hdr && hdr < (void *)GET(HDR_TO_NEXT(tmp))) break;
        tmp = (void *)GET(HDR_TO_NEXT(tmp));
      }
      PUT(HDR_TO_PREV(hdr), tmp);
      PUT(HDR_TO_NEXT(hdr), GET(HDR_TO_NEXT(tmp)));
      PUT(HDR_TO_PREV(GET(HDR_TO_NEXT(tmp))), hdr);
      PUT(HDR_TO_NEXT(tmp), hdr);
    }
  }
}

/*
 * coalesce - coalesce adjacent free blocks.
 */
static void *coalesce(void *hdr)
{
  void *new_hdr = hdr;
  unsigned char prev_alloc = GET_ALLOC(HDR_TO_ADJ_FTR(hdr));
  unsigned char next_alloc = GET_ALLOC(FTR_TO_ADJ_HDR(HDR_TO_FTR(hdr)));
  size_t size = GET_SIZE(hdr);
  if (prev_alloc && next_alloc) {
    insert_to_seglist(hdr);
    return hdr;
  }
  if (!next_alloc) {
    size += GET_SIZE(FTR_TO_ADJ_HDR(HDR_TO_FTR(hdr)));
    remove_from_seglist(FTR_TO_ADJ_HDR(HDR_TO_FTR(hdr)));
  }
  if (!prev_alloc) {
    new_hdr = FTR_TO_HDR(HDR_TO_ADJ_FTR(hdr));
    size += GET_SIZE(new_hdr);
    remove_from_seglist(new_hdr);
  }
  // remove_from_seglist(hdr);
  PUT(new_hdr, PACK(size, 0));
  PUT(HDR_TO_FTR(new_hdr), PACK(size, 0));
  insert_to_seglist(new_hdr);
#ifdef DEBUG
  mm_check();
#endif
  return new_hdr;
}

/*
 * extend_heap - extend heap over the given size.
 * fetch a memory, re-fill the epilogue, and coalesce free blocks. size is
 * automatically aligned.
 */
static void *extend_heap(size_t size)
{
  void *hdr;
  size = ALIGN(size);
  if ((long)(hdr = FBP_TO_HDR(mem_sbrk(size))) == -1)
    return NULL;
  PUT(hdr, PACK(size, 0));
  PUT(HDR_TO_FTR(hdr), PACK(size, 0));
  // insert_to_seglist(hdr);
  PUT(BOUND_HI-WSIZE, PACK(0, 1));
  PUT(BOUND_HI-2*WSIZE, PACK(0, 1));
  PUT(BOUND_HI-3*WSIZE, PACK(0, 1));
  return coalesce(hdr);
}

/*
 * mm_init - initialize the malloc package.
 * first, allocate list of seglists. then, fill prologue and epilogue with 0x1
 * (zero-sized allocated block). finally, extend the heap over the pagesize.
 */
int mm_init(range_t **ranges)
{
  /* YOUR IMPLEMENTATION */
  int i;
  seglists = mem_sbrk(ALIGN(WSIZE*N_SEGLIST));
  if (seglists == (void *)-1)
    return -1;
  for (i=0; i<N_SEGLIST; ++i) seglists[i] = NULL;
  void *tmp = mem_sbrk(4*WSIZE);
  if (tmp == (void *)-1)
    return -1;
  for (i=0; i<4; ++i) PUT(tmp + i*WSIZE, PACK(0, 1));
  if (extend_heap(mem_pagesize()) == NULL)
    return -1;

  /* DON't MODIFY THIS STAGE AND LEAVE IT AS IT WAS */
  gl_ranges = ranges;

  return 0;
}

/*
 * find_fit - find a free block which can allocate a block of given size.
 * if there is no fitting block in the corresponding seglist, then the
 * function search the next seglist. first fit criteria is used.
 */
static void *find_fit(size_t size)
{
  int idx;
  void *hdr;
  for (idx = size_to_index(size); idx < N_SEGLIST; ++idx) {
    hdr = seglists[idx];
    if (hdr == NULL) continue;
    do {
      if (size <= GET_SIZE(hdr)) {
        return hdr;
      }
      hdr = (void *)GET(HDR_TO_NEXT(hdr));
    } while (hdr != seglists[idx]);
  }
  return NULL;
}

/*
 * place - allocate a block at given address with given size.
 * split the block if necessary.
 */
static void place(void *hdr, size_t size)
{
  size_t prev_size = GET_SIZE(hdr);
  remove_from_seglist(hdr);
  if (prev_size - size > 4*WSIZE) {
    PUT(HDR_TO_FTR(hdr), PACK(prev_size - size, 0));
    PUT(hdr, PACK(size, 1));
    PUT(HDR_TO_FTR(hdr), PACK(size, 1));
    PUT(FTR_TO_ADJ_HDR(HDR_TO_FTR(hdr)), PACK(prev_size - size, 0));
    insert_to_seglist((void *)FTR_TO_ADJ_HDR(HDR_TO_FTR(hdr)));
  } else {
    PUT(hdr, PACK(prev_size, 1));
    PUT(HDR_TO_FTR(hdr), PACK(prev_size, 1));
  }
#ifdef DEBUG
  mm_check();
#endif
}

/*
 * mm_malloc - allocate a block by incrementing the brk pointer.
 * always allocate a block whose size is a multiple of the alignment.
 */
void* mm_malloc(size_t size)
{
  /* YOUR IMPLEMENTATION */
  void *hdr;
  if (size == 0) return NULL;
  else size = ALIGN(size + 2*WSIZE);
  // if (size == 4*WSIZE) size = ALIGN(size + 2*WSIZE);
  if ((hdr = find_fit(size)) != NULL) {
    place(hdr, size);
    return HDR_TO_ABP(hdr);
  }
  if ((hdr = extend_heap(MAX(size, mem_pagesize()))) == NULL)
    return NULL;
  place(hdr, size);
  return HDR_TO_ABP(hdr);
}

/*
 * mm_free - freeing a block does nothing.
 * raise a handle_double_free if the operation is invalid.
 */
void mm_free(void *bp)
{
  /* YOUR IMPLEMENTATION */
  void *hdr;
  size_t size;
  if ((unsigned int)bp%ALIGNMENT != 0) handle_double_free();
  hdr = ABP_TO_HDR(bp);
  if (BOUND_LO >= hdr || HDR_TO_FTR(hdr) >= BOUND_HI) handle_double_free();
  if (GET(hdr) != GET(HDR_TO_FTR(hdr))) handle_double_free();
  if (GET_ALLOC(hdr) != 1) handle_double_free();
  size = GET_SIZE(hdr);
  PUT(hdr, PACK(size, 0));
  PUT(HDR_TO_FTR(hdr), PACK(size, 0));
  // insert_to_seglist(hdr);
  coalesce(hdr);

  /* DON't MODIFY THIS STAGE AND LEAVE IT AS IT WAS */
  if (gl_ranges)
    remove_range(gl_ranges, bp);
}

/*
 * mm_realloc - empty implementation; YOU DO NOT NEED TO IMPLEMENT THIS
 */
void* mm_realloc(void *ptr, size_t t)
{
  return NULL;
}

/*
 * mm_exit - finalize the malloc package.
 * every remaining allocated blocks will be freed.
 */
void mm_exit(void)
{
  void *foo, *bar;
  foo = BOUND_LO + ALIGN(WSIZE*N_SEGLIST) + WSIZE;
  while (GET_SIZE(foo) > 0) {
    if (GET_ALLOC(foo) == 1) {
      bar = foo;
      while (GET_ALLOC(bar) != 1) {
        bar = FTR_TO_ADJ_HDR(HDR_TO_FTR(bar));
      }
      mm_free(HDR_TO_ABP(foo));
      foo = bar;
    } else {
      foo = FTR_TO_ADJ_HDR(HDR_TO_FTR(foo));
    }
  }
#ifdef DEBUG
  mm_check();
#endif
}
