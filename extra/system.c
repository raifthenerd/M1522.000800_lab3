#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <unistd.h>
#include <sys/wait.h>

int main()
{
  int *tmp;
  if (fork() == 0) {
    printf("case 1: normal execution - expect no error\n");
    tmp = malloc(sizeof(int)); printf("%p\n", tmp);
    free(tmp);
    exit(0);
  } else {
    wait(NULL);
    printf("case 1: ended...\n\n");
  }
  if (fork() == 0) {
    printf("case 2: double free - expect an error & handle double free\n");
    tmp = malloc(sizeof(int)); printf("%p\n", tmp);
    free(tmp);
    free(tmp);
    exit(0);
  } else {
    wait(NULL);
    printf("case 2: ended...\n\n");
  }
  if (fork() == 0) {
    printf("case 3: unallocated free - expect an error & handle double free\n");
    tmp = malloc(sizeof(int)); printf("%p\n", tmp);
    free(tmp);
    free(tmp+64);
    exit(0);
  } else {
    wait(NULL);
    printf("case 3: ended...\n\n");
  }
  if (fork() == 0) {
    printf("case 4: no free - expect a memory leak\n");
    tmp = malloc(sizeof(int)); printf("%p\n", tmp);
    exit(0);
  } else {
    wait(NULL);
    printf("case 4: ended...\n\n");
  }

  return 0;
}
