#include "mc.h"

static void *(*mallocp)(size_t size);
static void (*freep)(void *ptr);

static void __attribute__ ((constructor)) init(void)
{
  char *error;
  mallocp = dlsym(RTLD_NEXT, "malloc");
  if ((error = dlerror()) != NULL) {
    fputs(error, stderr);
    exit(1);
  }
  freep = dlsym(RTLD_NEXT, "free");
  if ((error = dlerror()) != NULL) {
    fputs(error, stderr);
    exit(1);
  }
  mem_init();
  mem_reset_brk();
  if (mm_init(NULL) < 0) {
    exit(1);
  }
}

void *malloc(size_t size)
{
  return mm_malloc(size);
}
void *__wrap_malloc(size_t size)
{
  return mallocp(size);
}

void free(void *ptr)
{
  mm_free(ptr);
}
void __wrap_free(void *ptr)
{
  freep(ptr);
}

static void __attribute__ ((destructor)) fini(void)
{
  unsigned int size;
  void *foo, *bar;
  foo = mem_heap_hi()-15;
  size = *(unsigned int *)foo & ~0x7;
  bar = foo - size;
  if (*(unsigned int *)bar != 1) {
    fputs("WARNING : Memory leak problem detected - try to solve...\n", stderr);
  }
  mm_exit();
  foo = mem_heap_hi()-15;
  size = *(unsigned int *)foo & ~0x7;
  bar = foo - size;
  if (*(unsigned int *)bar != 1) {
    fputs("ERROR : Memory leak problem cannot be solved.\n", stderr);
    exit(1);
  }
}
